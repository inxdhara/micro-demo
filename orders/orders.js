const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const axios = require('axios');
const Order = require('./Order');

// Load mongoose
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/ordersservice', () => {
    console.log('Connected to DB');
});

app.post('/order', (req, res) => {
    let newOrder = new Order({
        CustomerId: mongoose.Types.ObjectId(req.body.CustomerId),
        BookId: mongoose.Types.ObjectId(req.body.BookId),
        initialDate: req.body.initialDate,
        deliveryDate: req.body.deliveryDate,
    });

    newOrder.save()
    .then(async (result) => {
        res.send('A new order created with sucess!');
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.get('/orders', (req, res) => {
    Order.find({})
    .then((result) => {
        res.json(result);
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.get('/order/:id', (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.send('Please provide ID');
    }
    Order.findById(req.params.id)
    .then((result) => {
        if (result) {
            // res.json(result);

            axios.get(`http://localhost:3001/customer/${order.CustomerId}`)
            .then((response) => {
                let orderObject = {customerName: response.data.name, bookTitle: ''}

                axios.get(`http://localhost:3000/book/${order.BookId}`)
                .then((bookData) => {
                    orderObject.bookTitle = bookData.data.title;
j
                    res.json(orderObject);
                });
            });

            res.send('Quick Response');
        } else {
            res.sendStatus(404);
        }
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.listen(3030, () => {
    console.log('Up and runing! -- This is our Orders service');
});