// load express
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Book = require("./Book");
app.use(bodyParser.json());

// Load mongoose
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/booksservice', () => {
    console.log('Connected to DB');
});

app.get('/', (req, res) => {
    res.send('This is our main endpoint!');
});

app.post('/book', (req, res) => {
    let newBook = new Book({
        title: req.body.title,
        author: req.body.author,
        numberPages: req.body.numberPages,
        publisher: req.body.publisher,
    })

    newBook.save()
    .then(async (result) => {
        // res.success(result);
        res.send('A new book created with sucess!');
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.get('/books', (req, res) => {
    Book.find({})
    .then((result) => {
        res.json(result);
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.get('/book/:id', (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.send('Please provide ID');
    }
    Book.findById(req.params.id)
    .then((result) => {
        if (result) {
            res.json(result);
        } else {
            res.sendStatus(404);
        }
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.delete('/book/:id', (req, res) => {
    Book.findOneAndRemove(req.params.id)
    .then((result) => {
        res.send('Book deleted successfully');
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });

});

app.listen(3001, () => {
    console.log('Up and runing! -- This is our Books service');
});