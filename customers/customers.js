// load express
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Customer = require("./Customer");
app.use(bodyParser.json());

// Load mongoose
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/customersservice', () => {
    console.log('Connected to DB');
});

app.get('/', (req, res) => {
    res.send('This is our main endpoint!');
});

app.post('/customer', (req, res) => {
    let newCustomer = new Customer({
        name: req.body.name,
        age: req.body.age,
        address: req.body.address,
    })

    newCustomer.save()
    .then(async (result) => {
        // res.success(result);
        res.send('A new customer created with sucess!');
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.get('/customers', (req, res) => {
    Customer.find({})
    .then((result) => {
        res.json(result);
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.get('/customer/:id', (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.send('Please provide ID');
    }
    Customer.findById(req.params.id)
    .then((result) => {
        if (result) {
            res.json(result);
        } else {
            res.sendStatus(404);
        }
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
});

app.delete('/customer/:id', (req, res) => {
    Customer.findOneAndRemove(req.params.id)
    .then((result) => {
        res.send('Customer deleted successfully');
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });

});

app.listen(3000, () => {
    console.log('Up and runing! -- This is our Customers service');
});